<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

class user extends ActiveRecord implements \yii\web\IdentityInterface
{
	public $model;
	public static function tableName(){
		return 'user';
	}
	
	public function rules()
	{
		return
		[
			[['name','username', 'password',],'string', 'max' =>255],
			[['name','username', 'password',], 'required'],
			[['username'], 'unique'],
		];
	}

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
        /*return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;*/
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        /*foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }*/
		
		throw new NotSupportedException('Not supported');
		
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username'=>$username]);
		
        /*foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;*/
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
     /*public function validatePassword($password)
    {
        return $this->password === $password;
    }*/
	public function validatePassword($password)
    {
        return $this->isCorrectHash($password, $this->password);
    }
	
	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}
	
	public static function getUsers()
	{
		$allusers = self::find()->all();
		$allusersArray = ArrayHelper::
					map($allusers, 'id', 'name');
		return $allusersArray;						
	}

	//hash password before saving
    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        return $return;
    }
}