<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;
use app\models\User;
use app\models\Category;
/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 3]) ?>
	
	<?= $form->field($model, 'category')->
				dropDownList(Category::getCategorys()) ?> 
    <!--?= $form->field($model, 'category')->textInput() ?-->

    <!--?= $form->field($model, 'author')->textInput(['maxlength' => true]) ?-->
	
	<?= $form->field($model, 'author')->
				dropDownList(User::getUsers()) ?> 

    <!--?= $form->field($model, 'status')->textInput() ?-->

   <?= $form->field($model, 'status')->
				dropDownList(Status::getStatuses()) ?> 

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
